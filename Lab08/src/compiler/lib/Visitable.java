package compiler.lib;

import compiler.lib.BaseASTVisitor;

public interface Visitable {

	<S> S accept(BaseASTVisitor<S> visitor);

}
