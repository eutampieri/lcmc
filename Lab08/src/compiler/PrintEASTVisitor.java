package compiler;

import compiler.lib.BaseEASTVisitor;
import compiler.lib.Node;

public class PrintEASTVisitor extends BaseEASTVisitor<Void> {
    public PrintEASTVisitor() {
        super(true);
    }
    @Override
    public Void visitNode(AST.ProgNode n) {
        printNode(n);
        visit(n.exp);
        return null;
    }

    @Override
    public Void visitNode(AST.IntNode n) {
        printNode(n, n.val.toString());
        return null;
    }

    @Override
    public Void visitNode(AST.PlusNode n) {
        printNode(n);
        visit(n.left);
        visit(n.right);
        return null;
    }

    @Override
    public Void visitNode(AST.TimesNode n) {
        printNode(n);
        visit(n.left);
        visit(n.right);
        return null;
    }

    @Override
    public Void visitNode(AST.EqualNode n) {
        printNode(n);
        visit(n.left);
        visit(n.right);
        return null;
    }

    @Override
    public Void visitNode(AST.BoolNode n) {
        printNode(n, n.val.toString());
        return null;
    }


    @Override
    public Void visitNode(AST.IfNode n) {
        printNode(n);
        visit(n.cond);
        visit(n.th);
        visit(n.el);
        return null;
    }

    @Override
    public Void visitNode(AST.PrintNode n) {
        printNode(n);
        visit(n.exp);
        return null;
    }

    //
    @Override
    public Void visitNode(AST.ProgLetInNode n) {
        printNode(n);
        for (Node dec : n.declist) visit(dec);
        visit(n.exp);
        return null;
    }

    @Override
    public Void visitNode(AST.BoolTypeNode n) {
        printNode(n);
        return null;
    }

    @Override
    public Void visitNode(AST.IntTypeNode n) {
        printNode(n);
        return null;
    }

    @Override
    public Void visitNode(AST.VarNode n) {
        printNode(n,n.id);
        visit(n.type);
        visit(n.exp);
        return null;
    }

    @Override
    public Void visitNode(AST.FunNode n) {
        printNode(n,n.id);
        visit(n.retType);
        for (AST.ParNode par : n.parlist) visit(par);
        for (Node dec : n.declist) visit(dec);
        visit(n.exp);
        return null;
    }

    @Override
    public Void visitNode(AST.IdNode n) {
        printNode(n,n.id);
        if(n.entry != null) {
            this.visit(n.entry);
        }
        return null;
    }

    @Override
    public Void visitNode(AST.CallNode n) {
        printNode(n,n.id);
        if(n.entry != null) {
            this.visit(n.entry);
        }
        for (Node arg : n.arglist) visit(arg);
        return null;
    }
    @Override
    public Void visitNode(AST.ParNode n) {
        printNode(n,n.id);
        this.visit(n.type);
        // for (Node arg : n.arglist) visit(arg);
        return null;
    }

    @Override
    public Void visitSTentry(STentry entry) {
    	printSTentry("nestlev "+entry.nl);
    	return null;
    }
}
