package compiler;

import java.security.InvalidKeyException;
import java.util.*;
import java.util.Map;

import compiler.AST.*;
import compiler.lib.*;
import compiler.lib.Node;

public class SymbolTableASTVisitor extends BaseASTVisitor<Void> {
	
	int stErrors=0;
	private List<Map<String, STentry>> symTable = new ArrayList<>();
	//private int nestingLevel=0;
	private int getNestingLevel() {
		return symTable.size() - 1;
	}

	private void addToCurrentSymbolTable(String key, STentry value) throws InvalidKeyException {
		if(this.symTable.get(this.getNestingLevel()).put(key, value) != null) {
			throw new InvalidKeyException("Variable was already declared!");
		}
	}

	private void enterScope() {
		this.symTable.add(new HashMap<>());
	}

	private void exitScope() {
		this.symTable.remove(this.getNestingLevel());
	}

	private STentry stLookup(String identifier) {
		for(int i = this.getNestingLevel(); i >= 0; i--) {
			STentry value = this.symTable.get(i).get(identifier);
			if(value != null) {
				return value;
			}
		}
		return null;
	}

	private void reportError(String message) {
		System.out.println(message);
		this.stErrors += 1;
	}
	//livello ambiente con dichiarazioni piu' esterno e' 0 (prima posizione ArrayList) invece che 1 (slides)
	//il "fronte" della lista di tabelle e' symTable.get(nestingLevel)

	SymbolTableASTVisitor() {}
	SymbolTableASTVisitor(boolean debug) {super(debug);} // p=true enables print for debugging

	@Override
	public Void visitNode(ProgNode n) {
		if (print) printNode(n);
		visit(n.exp);
		return null;
	}
	
	@Override
	public Void visitNode(IntNode n) {
		if (print) printNode(n, n.val.toString());
		return null;
	}
	
	@Override
	public Void visitNode(PlusNode n) {
		if (print) printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}
	
	@Override
	public Void visitNode(TimesNode n) {
		if (print) printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}
	
	@Override
	public Void visitNode(EqualNode n) {
		if (print) printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}
	
	@Override
	public Void visitNode(BoolNode n) {
		if (print) printNode(n, n.val.toString());
		return null;
	}
	
	@Override
	public Void visitNode(IfNode n) {
		if (print) printNode(n);
		visit(n.cond);
		visit(n.th);
		visit(n.el);
		return null;
	}
	
	@Override
	public Void visitNode(PrintNode n) {
		if (print) printNode(n);
		visit(n.exp);
		return null;
	}

//
	@Override
	public Void visitNode(ProgLetInNode n) {
		if (print) printNode(n);
		// Add the global symbol table
		this.enterScope();
		for (Node dec : n.declist) visit(dec);
		visit(n.exp);
		this.exitScope();
		return null;
	}

	@Override
	public Void visitNode(VarNode n) {
		if (print) printNode(n,n.id);
		visit(n.exp);
		STentry entry = new STentry(this.getNestingLevel());
		try {
			this.addToCurrentSymbolTable(n.id, entry);
		} catch (InvalidKeyException e) {
            this.reportError("Var id " + n.id + " at line " + n.getLine() + " already declared!");
        }
        return null;
	}

	@Override
	public Void visitNode(FunNode n) {
		if (print) printNode(n,n.id);
		STentry entry = new STentry(this.getNestingLevel());
		try {
			this.addToCurrentSymbolTable(n.id, entry);
		} catch (InvalidKeyException e) {
			this.reportError("Fun id " + n.id + " at line " + n.getLine() + " already declared!");
		}
		this.enterScope();

		for(ParNode par: n.parlist) {
			try {
				entry = new STentry(this.getNestingLevel());
				this.addToCurrentSymbolTable(par.id, entry);
			} catch (InvalidKeyException e) {
				this.reportError("Par id " + par.id + " at line " + par.getLine() + " already declared!");
			}
		}
		// for (ParNode par : n.parlist) visit(par);
		for (Node dec : n.declist) visit(dec);
		visit(n.exp);
		this.exitScope();
		return null;
	}

//	private STentry stLookup(String id) {}
	
	@Override
	public Void visitNode(IdNode n) {
		if (print) printNode(n);
		STentry entry = this.stLookup(n.id);
		if(entry == null) {
			this.reportError("Undeclared var or par at line " + n.getLine() + ": " + n.id);
		} else {
			n.entry = entry;
		}
		return null;
	}

	@Override
	public Void visitNode(CallNode n) {
		if (print) printNode(n);
		for (Node arg : n.arglist) visit(arg);
		STentry entry = this.stLookup(n.id);
		if(entry == null) {
			this.reportError("Undeclared fun at line " + n.getLine() + ": " + n.id);
		} else {
			n.entry = entry;
		}
		return null;
	}
}
