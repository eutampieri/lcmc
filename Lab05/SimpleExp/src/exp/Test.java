package exp;
import java.io.*;
import org.antlr.v4.runtime.*;

public class Test {
    public static void main(String[] args) throws Exception {

        String fileName = ".." + File.separator + "prova.txt";
     
        CharStream chars = CharStreams.fromFileName(fileName);
        SimpleExpLexer lexer = new SimpleExpLexer(chars);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SimpleExpParser parser = new SimpleExpParser(tokens);
        
        //parser.prog();
        
        System.out.println("You had: "+lexer.getNumberOfLexicalErrors()+ " lexical errors and "+
                           parser.getNumberOfSyntaxErrors()+" syntax errors.");

        if (lexer.getNumberOfLexicalErrors()+parser.getNumberOfSyntaxErrors() > 0) {
            System.exit(1);
        }

        System.out.println("Calculating expression");

        SimpleCalcSTVisitor visitor = new SimpleCalcSTVisitor();
        System.out.println("The result is: " + visitor.visit(parser.prog()));

    }
}
