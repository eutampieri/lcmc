grammar SimpleExp;

// Add an attribute to the Lexer class
@lexer::members {
    private int lexicalErrors = 0;

    public final int getNumberOfLexicalErrors() {
        return this.lexicalErrors;
    }
}


// PARSER RULES

//    v Add exp followed by EOF to parse all file
prog: exp EOF {System.out.println("Parsing finished!");};

// Grammar
// Disambiguate grammar by ordering productions
exp : exp TIMES exp #expProdTimes // <assoc=right> exp TIMES exp
    | <assoc=right> exp MINUS exp #expProdMinus //  <assoc=right> exp PLUS exp
    | LPAR exp RPAR #expProdPar
    | NUM #expProdNum;


// LEXER RULES

MINUS    : '-';
TIMES   : '*';
LPAR    : '(';
RPAR    : ')';
NUM     : '0' | ('1'..'9') ('0'..'9')* ;

//                                       Match but don't send to parser
WHITESP : (' ' | '\t' | '\n' | '\r')+ -> channel(HIDDEN);

//              v disable maximal matching otherwise this will not parse correctly 6/*f*/+/*a*/7
COMMENT : '/*'.*?'*/'-> channel(HIDDEN);

// Last because order influences priority
ERR     : . {
    // This code gets executed on match
    System.out.println("Invalid char: "+ getText());
    // Increment lexical errors
    this.lexicalErrors += 1;
} -> channel(HIDDEN);
