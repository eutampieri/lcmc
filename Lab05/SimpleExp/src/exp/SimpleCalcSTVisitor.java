package exp;

public class SimpleCalcSTVisitor extends SimpleExpBaseVisitor<Integer>{
    @Override
    public Integer visitProg(SimpleExpParser.ProgContext ctx) {
        return this.visit(ctx.exp());
    }

    @Override
    public Integer visitExpProdMinus(SimpleExpParser.ExpProdMinusContext ctx) {
        return this.visit(ctx.exp(0)) - this.visit(ctx.exp(1));
    }

    @Override
    public Integer visitExpProdNum(SimpleExpParser.ExpProdNumContext ctx) {
        return Integer.parseInt(ctx.NUM().getText());
    }

    @Override
    public Integer visitExpProdPar(SimpleExpParser.ExpProdParContext ctx) {
        return this.visit(ctx.exp());
    }

    @Override
    public Integer visitExpProdTimes(SimpleExpParser.ExpProdTimesContext ctx) {
        return this.visit(ctx.exp(0)) * this.visit(ctx.exp(1));
    }
}
