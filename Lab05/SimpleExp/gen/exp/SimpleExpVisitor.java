// Generated from C:/Users/eugenio.tampieri/OneDrive - Alma Mater Studiorum UniversitÓ di Bologna/LCMC/Lab05/SimpleExp/src/exp\SimpleExp.g4 by ANTLR 4.12.0
package exp;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SimpleExpParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SimpleExpVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SimpleExpParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(SimpleExpParser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expProdNum}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpProdNum(SimpleExpParser.ExpProdNumContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expProdMinus}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpProdMinus(SimpleExpParser.ExpProdMinusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expProdPar}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpProdPar(SimpleExpParser.ExpProdParContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expProdTimes}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpProdTimes(SimpleExpParser.ExpProdTimesContext ctx);
}