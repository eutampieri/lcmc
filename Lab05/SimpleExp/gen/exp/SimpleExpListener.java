// Generated from C:/Users/eugenio.tampieri/OneDrive - Alma Mater Studiorum UniversitÓ di Bologna/LCMC/Lab05/SimpleExp/src/exp\SimpleExp.g4 by ANTLR 4.12.0
package exp;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SimpleExpParser}.
 */
public interface SimpleExpListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SimpleExpParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(SimpleExpParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link SimpleExpParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(SimpleExpParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expProdNum}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterExpProdNum(SimpleExpParser.ExpProdNumContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expProdNum}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitExpProdNum(SimpleExpParser.ExpProdNumContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expProdMinus}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterExpProdMinus(SimpleExpParser.ExpProdMinusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expProdMinus}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitExpProdMinus(SimpleExpParser.ExpProdMinusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expProdPar}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterExpProdPar(SimpleExpParser.ExpProdParContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expProdPar}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitExpProdPar(SimpleExpParser.ExpProdParContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expProdTimes}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterExpProdTimes(SimpleExpParser.ExpProdTimesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expProdTimes}
	 * labeled alternative in {@link SimpleExpParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitExpProdTimes(SimpleExpParser.ExpProdTimesContext ctx);
}