package exp;

//import org.antlr.v4.runtime.tree.*;
import exp.SimpleExpParser.*;

public class SimpleCalcSTVisitor extends SimpleExpBaseVisitor<Integer> {
	
//	@Override
//	public Integer visit(ParseTree x) {
//		String temp=indent;
//		indent=(indent==null)?"":indent+"  ";
//		int result = super.visit(x);
//		indent=temp;
//		return result; 
//	}
	
	@Override
	public Integer visitProg(ProgContext ctx) {
		System.out.println("prog");
		return visit( ctx.exp() );
	}

	@Override
	public Integer visitExpProd1(ExpProd1Context ctx) {
		System.out.println("exp: prod1 with TIMES");
			return visit( ctx.exp(0) ) * visit( ctx.exp(1) );
	}
	
}

//	int res= Integer.parseInt(ctx.NUM().getText());

