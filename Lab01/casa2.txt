Non è possibile realizzare un DFA che controlli le somme binarie con numero
arbitrario di bit in quanto non possiamo sapere a priori quanto sarà lungo
ogni numero e non possiamo quindi creare un automa perché non ci è possibile
memorizzare ed accedere successivamente le cifre già lette. 