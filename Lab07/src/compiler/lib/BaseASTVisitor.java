package compiler.lib;

import compiler.AST.*;

public class BaseASTVisitor<T>{
	private String indent;
	protected final boolean print;
	String extractNodeName(String s) { // s is in the form compiler.AST$NameNode
		return s.substring(s.lastIndexOf('$') + 1, s.length() - 4);
	}

	public T visit(Node<T> n) {
		String temp = indent;
		indent = (indent == null) ? "" : indent + "  ";
		T result = n.accept(this);
		indent = temp;
		return result;
	}

	protected BaseASTVisitor(boolean debug) {
		this.print = debug;
	}
	protected BaseASTVisitor() {
		this(false);
	}

	public T visitNode(ProgNode<T> n) {
		throw new UnimplException();
	}
	public T visitNode(PlusNode<T> n) {
		throw new UnimplException();
	}
	public T visitNode(TimesNode<T> n) {
		throw new UnimplException();
	}
	public T visitNode(IntNode<T> n) {
		throw new UnimplException();
	}
	public T visitNode(EqualsNode<T> n) {
		throw new UnimplException();
	}
	public T visitNode(BoolNode<T> n) {
		throw new UnimplException();
	}
	public T visitNode(IfNode<T> n) {
		throw new UnimplException();
	}
	public T visitNode(PrintNode<T> n) {
		throw new UnimplException();
	}
	protected void printNode(Node<T> n) {
		System.out.println(indent + extractNodeName(n.getClass().getName()));
	}

	protected void printNode(Node<T> n, String s) {
		System.out.println(indent + extractNodeName(n.getClass().getName()) + ": " + s);
	}
}






//throw new UnimplException();

//	protected boolean print=false;
//	protected BaseASTVisitor() {}
//	protected BaseASTVisitor(boolean p) { print=p; }

