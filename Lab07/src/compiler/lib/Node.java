package compiler.lib;

import compiler.lib.BaseASTVisitor;

public interface Node<T> {
    T accept(BaseASTVisitor<T> visitor);
}
