package compiler;

import compiler.AST.*;
import compiler.lib.*;

import java.util.Objects;

public class CalcASTVisitor extends BaseASTVisitor<Integer> {
    @Override
    public Integer visitNode(ProgNode<Integer> n) {
	   if (print) printNode(n);
        return visit(n.exp);
    }

    @Override
    public Integer visitNode(PlusNode<Integer> n) {
		if (print) printNode(n);
        return visit(n.left) + visit(n.right);
    }

    @Override
    public Integer visitNode(TimesNode<Integer> n) {
		if (print) printNode(n);
        return visit(n.left) * visit(n.right);
    }

    @Override
    public Integer visitNode(IntNode<Integer> n) {
		if (print) printNode(n,n.val.toString());
        return n.val;
    }

    @Override
    public Integer visitNode(EqualsNode<Integer> n) {
        if (print) printNode(n);
        return Objects.equals(visit(n.lhs), visit(n.rhs)) ? 1 : 0;
    }

    @Override
    public Integer visitNode(BoolNode<Integer> n) {
        if (print) printNode(n);
        return n.val ? 1 : 0;
    }

    @Override
    public Integer visitNode(IfNode<Integer> n) {
        if(print) printNode(n);
        if(visit(n.condition) == 1) {
            return visit(n.trueBranch);
        } else {
            return visit(n.falseBranch);
        }
    }
    public Integer visitNode(PrintNode<Integer> n) {
        if(print) printNode(n);
        System.out.println(visit(n.exp));
        return null;
    }


    CalcASTVisitor() {
        super(false);
    }

    CalcASTVisitor(boolean debug) {
        super(debug);
    } // enables print for debugging
}
