package compiler;

import compiler.lib.Node;
import org.antlr.v4.runtime.tree.*;
import compiler.FOOLParser.*;
//import compiler.AST.*;


public class ASTGenerationSTVisitor extends FOOLBaseVisitor<Node> {

	String indent;
	
    @Override
	public Node visit(ParseTree t) {             //visit now returns Node
        String temp=indent;
        indent=(indent==null)?"":indent+"  ";
        Node result = super.visit(t);
        indent=temp;
        return result;       
	}

	@Override
	public Node visitProg(ProgContext c) {
		System.out.println(indent+"prog");
 		return new AST.ProgNode(visit(c.exp()));
	}
	
	@Override
	public Node visitTimes(TimesContext c) {       //modified production tags
		System.out.println(indent+"exp: prod with TIMES");
		return new AST.TimesNode(visit(c.exp(0)), visit(c.exp(1)));
	}

	@Override
	public Node visitPlus(PlusContext c) {
		System.out.println(indent+"exp: prod with PLUS");
		return new AST.PlusNode(visit(c.exp(0)), visit(c.exp(1)));
	}

	@Override
	public Node visitPars(ParsContext c) {
		System.out.println(indent+"exp: prod with LPAR RPAR");
		return visit(c.exp());
	}

	@Override
	public Node visitInteger(IntegerContext c) {
		int res=Integer.parseInt(c.NUM().getText());
		res *= c.MINUS() == null ? 1 : -1;
		System.out.println(indent+"exp: prod with "+(res < 0 ? "MINUS " : "")+"NUM "+res);
		return new AST.IntNode(res);
	}

	@Override
	public Node visitEq(EqContext c) {
		System.out.println(indent+"exp: prod EQ");
		return new AST.EqualsNode(visit(c.exp(0)), visit(c.exp(1)));
	}
	@Override
	public Node visitTrue(TrueContext c) {
		System.out.println(indent+"exp: prod TRUE");
		return new AST.BoolNode(true);
	}

	@Override
	public Node visitFalse(FalseContext c) {
		System.out.println(indent+"exp: prod FALSE");
		return new AST.BoolNode(false);
	}

	public Node visitIf(IfContext c) {
		System.out.println(indent+"exp: prod IF");
		return new AST.IfNode(visit(c.exp(0)), visit(c.exp(1)), visit(c.exp(2)));
	}

	public Node visitPrint(PrintContext c) {
		System.out.println(indent+"exp: prod PRINT");
		return new AST.PrintNode(visit(c.exp()));
	}

}
