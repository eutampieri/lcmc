package compiler;

import compiler.lib.BaseASTVisitor;
import compiler.lib.Node;

public class AST {
	public static class ProgNode<T> implements Node<T> {
		Node<T> exp;
		ProgNode(Node<T> e) { exp=e; }

		@Override
		public T accept(BaseASTVisitor<T> visitor) {
			return visitor.visitNode(this);
		}
	}

	public static class PlusNode<T> implements Node<T> {
		Node<T> left;
		Node<T> right;
		PlusNode(Node<T> l, Node<T> r) { left=l; right=r; }

		@Override
		public T accept(BaseASTVisitor<T> visitor) {
			return visitor.visitNode(this);
		}
	}

	public static class TimesNode<T> implements Node<T> {
		Node<T> left;
		Node<T> right;
		TimesNode(Node<T> l, Node<T> r) { left=l; right=r; }

		@Override
		public T accept(BaseASTVisitor<T> visitor) {
			return visitor.visitNode(this);
		}
	}

	public static class IntNode<T> implements Node<T> {
		Integer val;
		IntNode(Integer n) { val=n; }

		@Override
		public T accept(BaseASTVisitor<T> visitor) {
			return visitor.visitNode(this);
		}
	}

	public static class EqualsNode<T> implements Node<T> {
		Node<T> lhs;
		Node<T> rhs;
		EqualsNode(Node<T> lhs, Node<T> rhs) { this.lhs = lhs; this.rhs = rhs; }

		@Override
		public T accept(BaseASTVisitor<T> visitor) {
			return visitor.visitNode(this);
		}
	}

	public static class BoolNode<T> implements Node<T> {
		Boolean val;
		BoolNode(Boolean n) { val=n; }

		@Override
		public T accept(BaseASTVisitor<T> visitor) {
			return visitor.visitNode(this);
		}
	}
	public static class IfNode<T> implements Node<T> {
		final Node<T> condition;
		final Node<T> trueBranch;
		final Node<T> falseBranch;
		IfNode(Node<T> condition, Node<T> trueBranch, Node<T> falseBranch) {
			this.condition = condition;
			this.falseBranch = falseBranch;
			this.trueBranch = trueBranch;
		}

		@Override
		public T accept(BaseASTVisitor<T> visitor) {
			return visitor.visitNode(this);
		}
	}

	public static class PrintNode<T> implements Node<T> {
		Node<T> exp;
		PrintNode(Node<T> e) { exp=e; }

		@Override
		public T accept(BaseASTVisitor<T> visitor) {
			return visitor.visitNode(this);
		}
	}

}
