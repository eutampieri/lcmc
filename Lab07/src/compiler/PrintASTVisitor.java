package compiler;

import compiler.AST.*;
import compiler.lib.BaseASTVisitor;
import compiler.lib.Node;

public class PrintASTVisitor<T> extends BaseASTVisitor<T> {

    @Override
    public T visitNode(ProgNode<T> n) {
        System.out.println("Prog");
        visit(n.exp);
        return null;
    }

    @Override
    public T visitNode(PlusNode<T> n) {
        printNode(n);
        visit(n.left);
        visit(n.right);
        return null;
    }

    @Override
    public T visitNode(TimesNode<T> n) {
        printNode(n);
        visit(n.left);
        visit(n.right);

        return null;
    }

    @Override
    public T visitNode(IntNode<T> n) {
        printNode(n, n.val.toString());
        return null;
    }

    @Override
    public T visitNode(EqualsNode<T> n) {
        printNode(n);
        visit(n.lhs);
        visit(n.rhs);
        return null;
    }

    @Override
    public T visitNode(BoolNode<T> n) {
        printNode(n, n.val.toString());
        return null;
    }

    @Override
    public T visitNode(IfNode<T> n) {
        printNode(n);
        visit(n.condition);
        visit(n.trueBranch);
        visit(n.falseBranch);
        return null;
    }
    public T visitNode(PrintNode<T> n) {
        printNode(n);
        visit(n.exp);
        return null;
    }

    PrintASTVisitor() {
        super(true);
        //super(true);
    }
}


