package svm;

public class ExecuteVM {

    public static final int CODESIZE = 10000;
    public static final int MEMSIZE = 10000;

    private final int[] code;
    private final int[] memory = new int[MEMSIZE];

    private int ip = 0;
    private int sp = MEMSIZE; //punta al top dello stack
    private int hp = 0;
    private int fp = sp;
    private int ra = 0;
    private int tm = 0;


    public ExecuteVM(int[] code) {
        this.code = code;
    }

    public void cpu() {
        while (true) {
            int bytecode = code[ip++]; // fetch
            int v1, v2;
            int address;
            switch (bytecode) {
                case SVMParser.PUSH -> {
                    v1 = code[ip++];
                    this.push(v1);
                }
                case SVMParser.POP -> {
                }
                case SVMParser.ADD -> {
                    v1 = pop();
                    v2 = pop();
                    push(v1 + v2);
                }
                case SVMParser.SUB -> {
                    v1 = pop();
                    v2 = pop();
                    push(v2 - v1);
                }
                case SVMParser.MULT -> {
                    v1 = pop();
                    v2 = pop();
                    push(v1 * v2);
                }
                case SVMParser.DIV -> {
                    v1 = pop();
                    v2 = pop();
                    push(v2 / v1);
                }
                case SVMParser.STOREW -> {
                    address = pop();
                    v2 = pop();
                    memory[address] = v2;
                }
                case SVMParser.LOADW -> {
                    address = pop();
                    push(memory[address]);
                }
                case SVMParser.BRANCH -> {
                    address = code[ip];
                    ip = address;
                }
                case SVMParser.BRANCHEQ -> {
                    v1 = pop();
                    v2 = pop();
                    address = code[ip++];
                    if (v1 == v2) {
                        ip = address;
                    }
                }
                case SVMParser.BRANCHLESSEQ -> {
                    v1 = pop();
                    v2 = pop();
                    address = code[ip++];
                    if (v2 <= v1) {
                        ip = address;
                    }
                }
                case SVMParser.JS -> {
                    v1 = pop();
                    ra = ip;
                    ip = v1;
                }
                case SVMParser.LOADRA -> {
                    push(ra);
                }
                case SVMParser.STORERA -> {
                    ra = pop();
                }
                case SVMParser.LOADTM -> {
                    push(tm);
                }
                case SVMParser.STORETM -> {
                    tm = pop();
                }
                case SVMParser.LOADFP -> {
                    push(fp);
                }
                case SVMParser.STOREFP -> {
                    fp = pop();
                }
                case SVMParser.COPYFP -> {
                    fp = sp;
                }
                case SVMParser.LOADHP -> {
                    push(hp);
                }
                case SVMParser.STOREHP -> {
                    hp = pop();
                }
                case SVMParser.PRINT -> {
                    System.out.println(Math.min(memory[sp], MEMSIZE - 1));
                }
                case SVMParser.HALT -> {
                    return;
                }
            }
            if (sp <= hp) {
                System.err.println("Halting execution: heap overflow");
                System.out.println("IP = " + ip);
                System.out.println("SP = " + sp);
                System.out.println("HP = " + hp);
                System.out.println("FP = " + fp);
                System.out.println("RA = " + ra);
                System.out.println("TM = " + tm);
                System.out.println("code[IP] = " + code[ip]);
                return;
            }
        }
    }

    private int pop() {
        return memory[sp++];
    }

    private void push(int v) {
        memory[--sp] = v;
    }

}