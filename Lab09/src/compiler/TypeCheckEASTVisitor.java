package compiler;

import compiler.AST.*;
import compiler.lib.*;
import compiler.exc.*;

import static compiler.lib.FOOLlib.*;

//visit(n) fa il type checking di un Node n e ritorna: 
//per una espressione, il suo tipo (oggetto BoolTypeNode o IntTypeNode)
//per una dichiarazione, "null"
public class TypeCheckEASTVisitor extends BaseEASTVisitor<TypeNode,TypeException> {

	TypeCheckEASTVisitor() {
		this(false);
	}
	TypeCheckEASTVisitor(boolean debug) { super(debug, true); } // enables print for debugging

	@Override
	public TypeNode visitNode(ProgLetInNode n) throws TypeException {
		if (print) printNode(n);
		for (Node dec : n.declist) {
			try {
				visit(dec);
			} catch (TypeException e) {
				System.out.println("Type checking error in a declaration: "+e.text);
			} catch (IncomplException e) {
				System.out.println("Incomplete AST");
			}
		}
		return visit(n.exp);
	}

	@Override
	public TypeNode visitNode(ProgNode n) throws TypeException {
		if (print) printNode(n);
		return visit(n.exp);
	}

	@Override
	public TypeNode visitNode(FunNode n) throws TypeException {
		if (print) printNode(n,n.id);
		visit(n.retType);
		for (Node dec : n.declist) {
			try {
				visit(dec);
			} catch (TypeException e) {
				System.out.println("Type checking error in a declaration: "+e.text);
			} catch (IncomplException e) {
				System.out.println("Incomplete AST");
			}

		}
		if(isSubtype(visit(n.exp), n.retType)) {
			return null;
		} else {
			throw new TypeException("Wrong return type for function " + n.id,n.getLine());
		}
	}

	@Override
	public TypeNode visitNode(VarNode n) throws TypeException {
		if (print) printNode(n,n.id);
		TypeNode t = visit(n.type);
		TypeNode e = visit(n.exp);
		if(isSubtype(e, n.type)) {
			return null;
		} else {
			throw new TypeException("Incompatible value for variable " + n.id,n.getLine());
		}
	}

	@Override
	public TypeNode visitNode(PrintNode n) throws TypeException {
		if (print) printNode(n);
		return visit(n.exp);
	}


	@Override
	public TypeNode visitNode(IfNode n) throws TypeException {
		if (print) printNode(n);
		TypeNode cond = visit(n.cond);
		TypeNode th = visit(n.th);
		TypeNode el = visit(n.el);

		if(!(isSubtype(th, el) || isSubtype(el, th))) {
			throw new TypeException("Incompatible types in then-else branches",n.getLine());
		} else if(!isSubtype(cond, new BoolTypeNode())) {
			throw new TypeException("Non boolean condition in if",n.getLine());
		} else {
			return isSubtype(th, el) ? el : th;
		}
	}

	@Override
	public TypeNode visitNode(EqualNode n) throws TypeException {
		if (print) printNode(n);
		TypeNode l = visit(n.left);
		TypeNode r = visit(n.right);
		if(isSubtype(l, r) || isSubtype(r, l)) {
			return new BoolTypeNode();
		} else {
			throw new TypeException("Incompatible types in equal",n.getLine());
		}
	}

	@Override
	public TypeNode visitNode(TimesNode n) throws TypeException {
		if (print) printNode(n);
		if(isSubtype(visit(n.left), new IntTypeNode()) &&
				isSubtype(visit(n.right), new IntTypeNode())) {
			return new IntTypeNode();
		} else {
			throw new TypeException("Non integers in multiplication",n.getLine());
		}
	}

	@Override
	public TypeNode visitNode(PlusNode n) throws TypeException {
		if (print) printNode(n);
		if(isSubtype(visit(n.left), new IntTypeNode()) &&
		isSubtype(visit(n.right), new IntTypeNode())) {
			return new IntTypeNode();
		} else {
			throw new TypeException("Non integers in sum",n.getLine());
		}
	}

	@Override
	public TypeNode visitNode(CallNode n) throws TypeException {
		if (print) printNode(n,n.id);
		if(visit(n.entry).getClass() != ArrowTypeNode.class) {
			throw new TypeException("Invocation of a non-function " + n.id, n.getLine());
		}
		ArrowTypeNode fromSymbolTable = (ArrowTypeNode)n.entry.type;
		if(n.arglist.size() != fromSymbolTable.parlist.size()) {
			throw new TypeException("Wrong number of parameters in the invocation of " + n.id, n.getLine());
		}
		for (int i = 0; i < n.arglist.size(); i++){
			Node arg = n.arglist.get(i);
			if(!isSubtype(visit(arg), fromSymbolTable.parlist.get(i))) {
				throw new TypeException("Wrong type for "+(i+1)+"-th parameter in the invocation of " + n.id, n.getLine());
			}
		}
		return fromSymbolTable.ret;
	}

	@Override
	public TypeNode visitNode(IdNode n) throws TypeException {
		if (print) printNode(n,n.id);
		TypeNode t = visit(n.entry);
		if(t.getClass() == ArrowTypeNode.class) {
			throw new TypeException("Wrong usage of function identifier "+n.id,n.getLine());
		}
		return t;
	}

	@Override
	public TypeNode visitNode(BoolNode n) {
		if (print) printNode(n,n.val.toString());
		return new BoolTypeNode();
	}

	@Override
	public TypeNode visitNode(IntNode n) {
		if (print) printNode(n,n.val.toString());
		return new IntTypeNode();
	}

	@Override
	public TypeNode visitNode(ArrowTypeNode n) throws TypeException {
		if (print) printNode(n);
		for (Node par: n.parlist) visit(par);
		visit(n.ret,"->"); //marks return type
		return null;
	}

	@Override
	public TypeNode visitNode(BoolTypeNode n) {
		if (print) printNode(n);
		return null;
	}

	@Override
	public TypeNode visitNode(IntTypeNode n) {
		if (print) printNode(n);
		return null;
	}

	@Override
	public TypeNode visitSTentry(STentry entry) throws TypeException {
		if (print) printSTentry("type");
		visit(entry.type);
		return entry.type;
	}

}
